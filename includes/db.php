<?php

// if(!defined('INCLUDE_CHECK')) die('You are not allowed to execute this file directly');




/* End config */

function getConnection() {
  /* Database config */

  $host = "localhost"; // Host name
  $username = "root"; // Mysql username
  $password = "root"; // Mysql password
  $db_name = "cnp"; // Database name


  $conn = mysqli_connect($host, $username, $password, $db_name) or die("cannot connect");
  return $conn;
}

// function returnMysqlDate($php_date) {
//   $date = date('Y-m-d', strtotime(str_replace('-', '/', $php_date)));
//   return $date;
// }

function formatDateForMysql($date) {
  if ($date==null) {
    return null;
  } else {
    $dateParts = date_parse($date);
    return $dateParts["year"]*10000 + $dateParts["month"]*100 + $dateParts["day"];
  }
}


function getResultsFromQuery($query){
  return getConnection()->query($query);
}

function retrieveCustomer($customer_id) {
  return getResultsFromQuery("CALL sp_findCustomer(" . $customer_id . ");");
}


function retrieveDestinations() {
  return getResultsFromQuery("SELECT * FROM destination");
}

function retrieveDestinationById($destination_id) {
  return  mysqli_fetch_assoc(getResultsFromQuery("SELECT * FROM destination WHERE destination_id = " . $destination_id . ";"));
}


function retrieveClasses() {
  return getResultsFromQuery("SELECT * FROM class");
}

function retrieveClassById($class_id) {
  return getResultsFromQuery("SELECT * FROM class WHERE class_id = " . $class_id . ";");
}


function retrieveFlight($destination_id, $date) {
  $sel_date = formatDateForMysql($date);
  $sql = "SELECT * FROM flight WHERE destination_id = " . $destination_id .
   " AND departure_date = " . $sel_date . ";";
  return  getResultsFromQuery($sql);
}

function retrieveFlightById($flight_id) {
  return getResultsFromQuery("CALL sp_getFlightDetailsById(" . $flight_id . ");");
}

// function retrieveFlight($flight_id) {
//   $sel_date = formatDateForMysql($date);
//   $sql = "SELECT * FROM flight WHERE flight_id = " . $flight_id . ";";
//   return  getResultsFromQuery($sql);
// }

function retrieveFlightDetails($flight_id) {
  return getResultsFromQuery("CALL sp_getFlightDetails(" . $flight_id . ");");
}


function seatAvailable($destination_id, $date, $class) {
  $sel_date = formatDateForMysql($date);
  $sql = "SELECT * FROM flight WHERE destination_id = " . $destination_id .
   " AND departure_date = " . $sel_date . ";";
   return  mysqli_fetch_assoc(getResultsFromQuery($sql));
}

function createFlight($destination_id, $date){
  $sel_date = formatDateForMysql($date);
  return getResultsFromQuery("CALL sp_createFlight(". $sel_date . ", "
                                                    . $destination_id ."); ");
}

function createSeat($flight_id, $class_id, $customer_id, $seat_type){
  if(checkSeatsAvailable($class_id, $flight_id)) {
    $res = getResultsFromQuery("CALL sp_createSeat(" . $flight_id . ", "
                                                   . $class_id . ", "
                                                   . $customer_id . ", "
                                                   . $seat_type . "); ");
    return true;
  } else {
    return false;
  }
}

function getMaxSeatsForClass($class_id) {
  $res =  mysqli_fetch_assoc(getResultsFromQuery("SELECT seat_count FROM class WHERE class_id = " . $class_id . ";"));
  if (!$res) {
    echo 'Could not run query: ' . mysqli_error();
    exit;
  }
  return $res["seat_count"];
}

function checkSeatsAvailable($class_id, $flight_id) {
  $total_seats = getMaxSeatsForClass($class_id);
  $sql = "SELECT count(*) count FROM seat WHERE flight_id = " . $flight_id .
      " AND class_id = " . $class_id . ";";

  $res =  mysqli_fetch_assoc(getResultsFromQuery($sql));

  if (!$res) {
    echo 'Could not run query: ' . mysqli_error();
    exit;
  }

  if ($res["count"] < $total_seats) {
    return true;
  } else {
    return false;
  }

}

function createUser($first_name, $last_name, $gender, $date_of_birth, $street, $town, $state, $postcode, $phone, $email) {

  $sql_create_frequent_flyer = "CALL sp_createFrequentFlyerEntry();";

  $res_ff = getResultsFromQuery($sql_create_frequent_flyer);

  $frequent_flyer_id = mysqli_fetch_assoc($res_ff)["frequent_flyer_id"];

  $fix_date = formatDateForMysql($date_of_birth);



  $sql_create_user = "CALL sp_registerUser('" . $first_name . "', '"
                                              . $last_name . "', '"
                                              . $gender . "', "
                                              . $fix_date . ", '"
                                              . $street . "', '"
                                              . $town . "', '"
                                              . $state . "', "
                                              . $postcode . ", '"
                                              . $phone . "', '"
                                              . $email . "', '"
                                              . $frequent_flyer_id . "');";

  $res = getResultsFromQuery($sql_create_user);

  return mysqli_fetch_assoc($res);
}

function retrieveCreditCards() {
  return getResultsFromQuery("SELECT * FROM credit_card");
}

function retrieveCustomerList() {
  return getResultsFromQuery("CALL sp_reportGetUsers();");
}

function retrieveWeeklyReport($reportDate) {
  $fix_date = formatDateForMysql($reportDate);
  return getResultsFromQuery("CALL sp_reportGetWeeklyReport('". $fix_date . "');");
}

function modifyFrequentFlyerPoints($frequent_flyer_id, $amount){
  return getResultsFromQuery("CALL sp_modifyFrequentFlyerPoints(". $frequent_flyer_id . ", "
                                                                 . $amount ."); ");
}

// mysql_query("SET names UTF8");

?>
