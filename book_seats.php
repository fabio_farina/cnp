<?php
  include('includes/db.php');

  $title="Make Payment";

?>

<html class="whole">
    <?php include('templates/header.php'); ?>
<body>
    <?php include('templates/navbar.php'); ?>
    <div class="container">
      <div class="content">
        <h2><?php echo $title; ?> </h2>
        <?php


            if(isset($_GET["class_id"]) && isset($_GET["flight_id"]) && isset($_GET["flight_type"])) {
              $class = $_GET["class_id"];
              $flight_id = $_GET["flight_id"];
              $flight_type = $_GET["flight_type"];

              $flight_details = mysqli_fetch_assoc(retrieveFlightDetails($flight_id));
              $class_details = mysqli_fetch_assoc(retrieveClassById($class));
              $cust_details = mysqli_fetch_assoc(retrieveCustomer($_SESSION["customer_details"]["customer_id"]));

              $flight_type_string = (($flight_type == 1) ? "One Way" : "Return");

              $money_cost = $flight_details["cost_to_destination"] * $flight_type * $class_details["cost_multiplier"];

              $freq_flyer_amt = $class_details["frequent_flyer_multiplier"] * $flight_details["destination_dist"] * $flight_type;
              $req_frequent_flyer = ($freq_flyer_amt * 4);


              if(isset($_GET["pay_type"]) && $_GET["pay_type"] == 1) {
                if($req_frequent_flyer > $cust_details["frequent_flyer_points"]) {
                  echo "<h3>You do not have enough frequent flyer points to make this purchase. Please pay by card.</h3>";
                  echo "<br />";
                  $_GET["pay_type"] = NULL;
                }
              }

            ?>

            <form action="#" method="get" class="form_large">
              <input type="radio" name="pay_type" value="1" >
              <label for="1">Frequent Flyers</label>
              <input type="radio" name="pay_type" value="2" >
              <label for="2">Credit Card</label>
              <input type="hidden" name="class_id" value="<?php echo $_GET["class_id"]; ?>" />
              <input type="hidden" name="flight_id" value="<?php echo $_GET["flight_id"]; ?>" />
              <input type="hidden" name="flight_type" value="<?php echo $_GET["flight_type"]; ?>" /><br />
              <input type="submit" value="Select" />
            </form>

            <?php
            if(!isset($_POST["mode"])) {
              if(isset($_GET["pay_type"])) {
                if($_GET["pay_type"]==1) {
                  $message = "Please Confirm flight details. You [" . $_SESSION["customer_details"]["first_name"] . "] are placing a booking for Destination: [" . $flight_details["destination_name"] .
                             "]. Departing on the date: [" . $flight_details["departure_date"] . "]. Flight Type: [" . $flight_type_string . "]. Class: [" .
                              $class_details["name"]. "]. Total Frequent Flyer Cost is: [" . $req_frequent_flyer . " points]";

            ?>

                  <form action="" method="post" onsubmit="return confirm('<?php echo $message; ?>')" class="form_large">
                    <h3>Frequent Flyer</h3>

                    <b>Your current amount:</b> <?php echo $cust_details["frequent_flyer_points"]; ?>
                    <br />

                    <b>Require Frequent Flyer Points:</b> <?php echo $req_frequent_flyer; ?>
                    <br />

                    <input type="hidden" name="mode" value="payment">
                    <input type="hidden" name="paymeth" value="freq">
                    <input type="hidden" name="flight_id" value="<?php echo $flight_id; ?>">
                    <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                    <input type="hidden" name="amount" value="<?php echo (-$req_frequent_flyer); ?>" />
                    <input type="submit" value="Make Payment" />
                    <input type="button" value="Cancel"  onclick="window.location='index.php'"/>
                  </form>

            <?php
                } else {
                  $message = "Please Confirm flight details. You [" . $_SESSION["customer_details"]["first_name"] . "] are placing a booking for Destination: [" . $flight_details["destination_name"] . "]. Departing on the date: [" .
                            $flight_details["departure_date"] . "]. Flight Type: [" . $flight_type_string . "]. Class: [" . $class_details["name"].
                            "]. Total Cost is: [$" . number_format($money_cost, 2) . "]";
            ?>

                  <form action="" method="post" onsubmit="return confirm('<?php echo $message; ?>')" class="form_large">
                    <b>Card Type: </b>
                    <select name="card_type" id="card_type">
                      <?php
                        $card_types = retrieveCreditCards();
                        while($row = mysqli_fetch_assoc($card_types)) {
                          echo '<option value="' . $row["card_id"] . '">' . $row["card_name"] . '</option>';
                        }
                      ?>
                    </select>

                    <b>Card Number: </b>
                    <input type="text" name="card_num" id="card_num" />

                    <b>Name on card: </b>
                    <input type="text" name="name_on_card" id="name_on_card" />


                    <b>Card Expiry:</b>
                    <br />
                    <b>Month</b>
                    <select name="month" id="month">
                      <option value="1">01</option>
                      <option value="2">02</option>
                      <option value="3">03</option>
                      <option value="4">04</option>
                      <option value="5">05</option>
                      <option value="6">06</option>
                      <option value="7">07</option>
                      <option value="8">08</option>
                      <option value="9">09</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>

                    <b>Year</b>
                    <select name="year" id="year">
                      <?php
                        $year = intval(date("Y"));
                        for ($x = $year; $x <= $year+20; $x++) {
                          echo "<option value='" . $x . "'>" . $x . "</option>";
                        }
                      ?>
                    </select>

                    <b>Security Verification</b>
                    <input type="text" name="security" id="security" />
                    <input type="hidden" name="mode" value="payment">
                    <input type="hidden" name="paymeth" value="card">
                    <input type="hidden" name="flight_id" value="<?php echo $flight_id; ?>">
                    <input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
                    <input type="hidden" name="amount" value="<?php echo $money_cost; ?>" />
                    <input type="submit" value="Make Payment" />
                    <input type="button" value="Cancel"  onclick="window.location='index.php'"/>

                  </form>


                <?php
                }
            }
        } else {
          $booked = createSeat($flight_id, $class, $_SESSION["customer_details"]["customer_id"], $flight_type);
          $booked = true;

          $amount = 0;
          if($_POST["paymeth"] == "card"){
            $amount = $freq_flyer_amt;
          } else if ($_POST["paymeth"]=="freq") {
            $amount = $_POST["amount"];
          }


          if($booked) {
            modifyFrequentFlyerPoints($_SESSION["customer_details"]["frequent_flyer_id"], $amount);
            echo "<h3>Enjoy your flight!</h3>";
          } else {
            echo "<h3>Something went wrong.</h3>";
          }
        }
      }
        ?>
      </div>
    </div>

</body>
</html>

<script>
  $('.navbar:first ul li:eq(1) a').attr("class", "active");

</script>
