<?php
  include('includes/db.php');

  $title="Search Flight Details";

?>

<html class="whole">
    <?php include('templates/header.php'); ?>
<body>
    <?php include('templates/navbar.php'); ?>

    <div class="container">
      <div class="content">
        <h2><?php echo $title; ?> </h2>
        <?php
          if(isset($_GET["dest"]) && isset($_GET["air_class"]) && isset($_GET["departure"]) && isset($_GET["flight_type"])) {

            $destination_id = $_GET["dest"];
            $class = $_GET["air_class"];
            $dep_date = $_GET["departure"];
            $flight_type = $_GET["flight_type"];

            createFlight($destination_id, $dep_date);
            $flight_result = mysqli_fetch_assoc(retrieveFlight($destination_id, $dep_date));


            $dest_details = retrieveDestinationById($destination_id);

        ?>
        <h2>Destination</h2>
        <h2><?php echo $dest_details["destination_name"]; ?></h2>
        Departure Date: <?php echo $dep_date; ?>
        <h3>Check seats are available</h3>
        <form action="" method="get" class="form_large">
          <input type="hidden" name="class_id" value="<?php echo $class; ?>" />
          <input type="hidden" name="flight_type" value="<?php echo $flight_type; ?>" />
          <input type="hidden" name="flight_id" value="<?php echo $flight_result["flight_id"]; ?>" />
          <input type="submit" value="Check Seats Available">
          <input type="button" value="Cancel"  onclick="window.location='index.php'" />
        </form>
        <?php
          }
          if(isset($_GET["class_id"]) && isset($_GET["flight_id"]) && isset($_GET["flight_type"])) {
            $class = $_GET["class_id"];
            $flight_id = $_GET["flight_id"];
            $flight_type = $_GET["flight_type"];

            $seat_available = checkSeatsAvailable($class, $flight_id);

            if (!$seat_available) {
              echo "<h3>No seats available. Sorry for the inconvenience.</h3>";
            } else {
              echo "</h3>Seat available for the requested flight. Please proceed to making a payment</h3>";
        ?>
            <h2>Would you like to place a booking for this flight?</h2>
            <form action="book_seats.php" method="get" class="form_large">
              <input type="hidden" name="class_id" value="<?php echo $class; ?>" />
              <input type="hidden" name="flight_id" value="<?php echo $flight_id; ?>" />
              <input type="hidden" name="flight_type" value="<?php echo $flight_type; ?>" />
              <input type="submit" value="Proceed to Payment" />
              <input type="button" value="Cancel"  onclick="window.location='index.php'"/>
            </form>

        <?php

            }

          }

        ?>
      </div>
    </div>
</body>
</html>

<script>
  $('.navbar:first ul li:eq(1) a').attr("class", "active");
</script>
