<?php
  include('includes/db.php');

  $title="Search Flight Details";

?>

<html class="whole">
    <?php include('templates/header.php'); ?>
<body>

    <?php include('templates/navbar.php'); ?>

    <div class="container">
      <div class="content">
        <h2><?php echo $title; ?> </h2>

        <?php
          if(!isset($_SESSION["customer_details"])) {
            die("<h3>You must be logged in to view this page. Please <a href='login.php'>Login</a> or <a href='register.php'>Register</a>.</h3>");
          }
          $cust_details = mysqli_fetch_assoc(retrieveCustomer($_SESSION["customer_details"]["customer_id"]));
        ?>
        <form action="" method="get" class="form_large">
          <b>Destination:</b>
          <select name="destination" id="destination">
            <?php
              $res = retrieveDestinations();
              $flight_data = array();
              while($row = mysqli_fetch_assoc($res)) {
                echo '<option value="' . $row["destination_id"] . '">' . $row["destination_name"] . '</option>';
                $flight_data[$row["destination_id"]] = $row;
              }
            ?>
          </select>
          <b>Class:</b>
          <select name="air_class" id="air_class">
            <?php
            $res = retrieveClasses();
            $class_data = array();
            while($row = mysqli_fetch_assoc($res)) {
              echo '<option value="' . $row["class_id"] . '">' . $row["name"] . '</option>';
              $class_data[$row["class_id"]] = $row;
            }
            ?>
          </select>

          <b>One Way or Return?</b>
          <br />
            <input type="radio" name="flight_type" value="1" >
            <label for="1">One Way</label>
            <input type="radio" name="flight_type" value="2" >
            <label for="2">Return</label>
          <br />

          <input type="submit" value="Select Destination">
        </form>
        <br />



        <?php

        if (isset($_GET["destination"]) && isset($_GET["air_class"]) && isset($_GET["flight_type"])) {
          $dest = $flight_data[$_GET["destination"]];
          $class = $class_data[$_GET["air_class"]];
          $flight_type = $_GET["flight_type"];

          $freq_flyer_amt = $class["frequent_flyer_multiplier"] * $dest["destination_dist"] * $flight_type;
          $req_frequent_flyer = ($freq_flyer_amt * 4);

          ?>
          <form method="get" action="check_seats.php" class="form_large">
              <h2>Destination Description:</h2>
              <p>Here is some information about the place you just selected</p>

              <img src="<?php echo $dest["destination_photo"]; ?>" alt="" />
              <br />
              <br />
              <p>
                <?php echo $dest["destination_description"]; ?>
              </p>

              <b>Departure Date:</b>
              <input type="text" name="departure" id="datepicker">
              <br />

              <b>Distance:</b> <span id="distance"><?php echo $dest["destination_dist"]; ?></span>km
              <br />

              <b>Points rewarded for this flight:</b><?php echo $freq_flyer_amt; ?> Frequent Flyer Points
              <br />

              <b>Points required for free flight:</b> <?php echo $req_frequent_flyer . ' Frequent Flyer Points required. You have ' . $cust_details["frequent_flyer_points"] . ' points'; ?>
              <br />
              <input type="hidden" name="dest" value="<?php echo $_GET["destination"]; ?>" />
              <input type="hidden" name="air_class" value="<?php echo $_GET["air_class"]; ?>" />
              <input type="hidden" name="flight_type" value="<?php echo $flight_type; ?>" />

              <input type="submit" value="Check Available" />
              <input type="button" value="Cancel"  onclick="window.location='index.php'" />
            </form>
        <?php
          }
        ?>
    </div>
  </div>
</body>
</html>

<script>
  $('.navbar:first ul li:eq(0) a').attr("class", "active");
</script>
