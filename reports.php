<?php
    $title="Reporting Information";
?>

<html class="whole">
  <?php include('templates/header.php'); ?>
  <body>
    <?php include('templates/navbar.php'); ?>
    <div class="container">
      <div class="content">
        <h2><?php echo $title; ?> </h2>
        <div class="form_large">
          <b>Select which report to run:</b>
          <select name="report" id="report" value="Select option">
            <option value="users.php">Show users</option>
            <option value="weekly_flights.php">Show flights report</option>
          </select>
          <input type="button" value="Open Report" onClick="navigateTo()"/>
        </div>
      </div>
    </div>
  </body>
</html>

<script>
  $('.navbar:first ul li:eq(2) a').attr("class", "active");

  function navigateTo(){
       document.location.href = document.querySelector('select[name="report"]').value;
  }

</script>
