<div class="login_bar">
  <?php
    if(isset($_SESSION["customer_details"])) {
      echo "<ul>";
      echo "<li><a href='customer_information.php'>Welcome, "  . $_SESSION["customer_details"]["first_name"] . "</a></li>";
      echo "<li><a href='logout.php'>Log Off</a></li>";
      echo "</ul>";
      // echo "Welcome, " . $_SESSION["customer_details"]["first_name"];
      // echo " | <a href='logout.php'>Log Off</a>";
    } else {
      echo "<ul>";
      echo "<li><a href='login.php'>Log in</a></li>";
      echo "<li><a href='register.php'>Register</a></li";
      echo "</ul>";
      // echo "<a href='login.php'>Log in</a> | <a href='register.php'>Register</a>";
    }
  ?>
</div>


<div class="title">
  <a href="index.php">
    <img src="resources/img/site_banner.png" alt="Site Banner" />
  </a>
</div>
<div class="navbar">
  <ul>
    <li><a href="index.php">Home</a></li>
    <li><a href="search_flights.php">Flights</a></li>
    <li><a href="reports.php">Reports</a></li>
    <li><a href="customer_information.php">Customer Info</a></li>
  </ul>
</div>
