<?php include('includes/authenticate.php'); ?>
<head>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" type="text/css" href="resources/navbar.css">
  <link rel="stylesheet" type="text/css" href="resources/containers.css">
  <link rel="stylesheet" type="text/css" href="resources/style.css">
  <script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>
  <title>
    <?php echo $title; ?>
  </title>
</head>
