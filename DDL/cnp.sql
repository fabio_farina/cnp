-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2015 at 04:04 PM
-- Server version: 10.0.18-MariaDB-log
-- PHP Version: 5.6.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cnp`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_createFlight`(IN departureDate VARCHAR(10), IN destinationId INT)
BEGIN
	INSERT INTO flight (flight_id, departure_date, destination_id) VALUES (NULL, departureDate, destinationId);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_createFrequentFlyerEntry`()
    NO SQL
BEGIN
	INSERT INTO frequent_flyer (frequent_flyer_points) VALUES (0);    
    SELECT LAST_INSERT_ID() frequent_flyer_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_createSeat`(IN `flightId` INT, IN `classId` INT, IN `customerId` INT, IN `seatType` INT)
BEGIN
	INSERT INTO seat (seat_id, flight_id, class_id, customer_id, type) VALUES (NULL, flightId, classId, customerId, seatType);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_findCustomer`(IN `customerId` INT)
BEGIN
	SELECT * FROM customer c
    LEFT OUTER JOIN frequent_flyer f ON c.frequent_flyer_id = f.frequent_flyer_id
    WHERE c.customer_id = customerId;    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getFlightDetails`(IN `flightId` INT)
    NO SQL
BEGIN
	SELECT * FROM flight f 
    LEFT OUTER JOIN destination d on f.destination_id = d.destination_id
    WHERE f.flight_id = flightId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getFlightDetailsById`(IN flightId INT)
BEGIN
	SELECT * FROM flight f 
    LEFT OUTER JOIN destination d on f.destination_id = d.destination_id
    LEFT OUTER JOIN seat s on f.flight_id = s.flight_id
    WHERE f.flight_id = flightId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_modifyFrequentFlyerPoints`(IN `frequentFlyerId` INT, IN `modifyAmount` INT)
    NO SQL
BEGIN

	UPDATE `frequent_flyer` 
    SET    `frequent_flyer_points` = `frequent_flyer_points` + modifyAmount 
    WHERE  `frequent_flyer_id` = frequentFlyerId;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_registerUser`(IN `firstName` VARCHAR(15), IN `lastName` VARCHAR(20), IN `gender` CHAR(1), IN `dateOfBirth` DATE, IN `street` VARCHAR(25), IN `town` VARCHAR(12), IN `state` VARCHAR(11), IN `postCode` INT(4), IN `phone` INT(10), IN `email` VARCHAR(25), IN `frequentFlyerId` INT)
BEGIN
	INSERT INTO customer (customer_id, last_name, first_name, gender, date_of_birth, street, town, state, postcode, phone, email, frequent_flyer_id, join_date) 
                VALUES   (NULL, lastName, firstName, gender, dateOfBirth, street, town, state, postCode, phone, email, frequentFlyerId, CURRENT_DATE);
	SELECT LAST_INSERT_ID() customer_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reportGetUsers`()
    NO SQL
BEGIN
	SELECT * FROM customer c
    LEFT OUTER JOIN frequent_flyer f ON c.frequent_flyer_id = f.frequent_flyer_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_reportGetWeeklyReport`(IN `startdate` DATE)
    NO SQL
BEGIN

SELECT f.departure_date "departure", SUM(c.cost_multiplier * d.cost_to_destination * s.type) "cost" FROM flight f
LEFT OUTER JOIN destination d ON f.destination_id = d.destination_id
LEFT OUTER JOIN seat s ON f.flight_id = s.flight_id
LEFT OUTER JOIN class c ON s.class_id = c.class_id
WHERE f.departure_date BETWEEN startdate and DATE_ADD(startdate, INTERVAL 7 DAY) AND "cost" IS NOT NULL
GROUP BY f.departure_date;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `class_id` int(11) NOT NULL COMMENT 'primary key for the class table',
  `name` varchar(15) NOT NULL COMMENT 'Name of class (i.e. BUSINESS, ECONOMY, etc)',
  `cost_multiplier` decimal(9,2) NOT NULL COMMENT 'Cost multiplier for class',
  `frequent_flyer_multiplier` decimal(10,1) NOT NULL COMMENT 'represents the amount of frequent flyer points the customer receives in this class',
  `seat_count` int(11) NOT NULL COMMENT 'Total number of seats in this class'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Table contains the class of the seat on the airline.';

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`class_id`, `name`, `cost_multiplier`, `frequent_flyer_multiplier`, `seat_count`) VALUES
(1, 'BUSINESS', '1.50', '1.5', 2),
(2, 'ECONOMY', '1.00', '1.0', 4);

-- --------------------------------------------------------

--
-- Table structure for table `credit_card`
--

CREATE TABLE IF NOT EXISTS `credit_card` (
  `card_id` int(11) NOT NULL COMMENT 'Primary key of credit card table',
  `card_name` varchar(25) NOT NULL COMMENT 'Name of card'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `credit_card`
--

INSERT INTO `credit_card` (`card_id`, `card_name`) VALUES
(1, 'VISA'),
(2, 'MasterCard'),
(3, 'American Express'),
(4, 'Diners');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL COMMENT 'Primary Key of Customer Table',
  `last_name` varchar(20) NOT NULL COMMENT 'Customer''s Last name',
  `first_name` varchar(15) NOT NULL COMMENT 'Customer''s first name',
  `gender` char(1) NOT NULL COMMENT 'Customer''s Gender (either M or F)',
  `date_of_birth` date NOT NULL COMMENT 'Customer''s Date of Birth',
  `street` varchar(25) NOT NULL COMMENT 'Customer''s street',
  `town` varchar(12) NOT NULL COMMENT 'Customer''s Town',
  `state` varchar(11) NOT NULL COMMENT 'Customer''s State (REF: STATE table)',
  `postcode` int(4) NOT NULL COMMENT 'Customer''s postcode',
  `phone` int(10) NOT NULL COMMENT 'Customer''s phone number',
  `email` varchar(25) NOT NULL COMMENT 'customer''s email',
  `frequent_flyer_id` int(11) NOT NULL COMMENT 'customer''s current frequent flyer amount',
  `join_date` date NOT NULL COMMENT 'date customer joined'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='This table contains all the customer''s details';

-- --------------------------------------------------------

--
-- Table structure for table `destination`
--

CREATE TABLE IF NOT EXISTS `destination` (
  `destination_id` int(11) NOT NULL COMMENT 'primary key of destination table',
  `destination_name` varchar(25) NOT NULL COMMENT 'name of the destination',
  `destination_dist` int(11) NOT NULL COMMENT 'distance from the nation''s capital (nauticle miles)',
  `cost_to_destination` decimal(9,2) NOT NULL COMMENT 'cost to get from canberra to destination and back',
  `destination_photo` varchar(40) NOT NULL,
  `destination_description` text NOT NULL COMMENT 'Description of the destination'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='This table contains details about the destination for the customer.';

--
-- Dumping data for table `destination`
--

INSERT INTO `destination` (`destination_id`, `destination_name`, `destination_dist`, `cost_to_destination`, `destination_photo`, `destination_description`) VALUES
(1, 'ADELAIDE', 525, '139.00', 'resources/img/destinations/adelaide.png', 'Adelaide is South Australia’s cosmopolitan coastal capital. Its ring of parkland on the River Torrens is home to renowned museums such as the Art Gallery of South Australia, displaying expansive collections including noted Indigenous art, and the South Australian Museum, devoted to natural history. The city''s Adelaide Festival is an annual international arts gathering with spin-offs including fringe and film events.'),
(2, 'BRISBANE', 516, '189.00', 'resources/img/destinations/brisbane.png', 'Brisbane, capital of Queensland, is a big, modern city set on the Brisbane River. Clustered in its South Bank cultural precinct are institutions including Queensland Museum and Sciencentre, with noted interactive exhibitions, and Queensland Gallery of Modern Art, among Australia''s major contemporary art museums. Looming over the city is Mt. Coot-tha, site of sprawling Brisbane Botanic Gardens, with panoramic views from its summit.'),
(3, 'SYDNEY', 127, '65.00', 'resources/img/destinations/sydney.png', 'Sydney, capital of New South Wales and one of Australia''s largest cities, is best known for its harbourfront Opera House, with a distinctive sail-like design. Massive Darling Harbour and Circular Quay are hubs of waterside life, with the towering, arched Harbour Bridge and esteemed Royal Botanic Gardens nearby. Sydney Tower’s 268m glass viewing platform, the Skywalk, offers 360-degree views of the city, harbour and suburbs.'),
(4, 'MELBOURNE', 254, '98.00', 'resources/img/destinations/melbourne.png', 'Melbourne, Victoria’s coastal capital, is a city of stately 19th-century buildings and tree-lined boulevards. Yet at its centre is the strikingly modern Federation Square development, with plazas, bars, restaurants and cultural events along the Yarra River. In Southbank, the Melbourne Arts Precinct is site of Arts Centre Melbourne – a performing arts complex – and National Gallery of Victoria, displaying Australian and Indigenous art.'),
(5, 'HOBART', 459, '115.00', 'resources/img/destinations/hobart.png', 'Hobart, the capital of Australia''s Tasmania island, sits on the River Derwent. At its fashionable Salamanca Place, old sandstone warehouses host galleries, pubs and cafes, and a popular market is held on Saturdays. Nearby is Battery Point, the historic residential district, with its narrow lanes and colonial-era cottages. The city''s backdrop is 1,270m-high Mt. Wellington, a hiking and cycling destination offering sweeping views.'),
(6, 'PERTH', 1668, '278.00', 'resources/img/destinations/perth.png', 'Perth, capital of Western Australia, sits where the Swan River meets the southwest coast. Its suburbs lie along sandy beaches, and the huge, riverside Kings Park and Botanic Garden on Mt. Eliza offer sweeping views of the city. The Perth Cultural Centre houses the state ballet and opera companies, and occupies its own central precinct, including a theatre, art galleries and the Western Australian Museum.'),
(7, 'DARWIN', 1695, '312.00', 'resources/img/destinations/darwin.png', 'Darwin is capital of the Northern Territory and gateway to Kakadu National Park, Australia’s largest. It''s known as a laid-back blend of frontier outpost and modern city. At its heart is the Smith Street pedestrian mall, notable for shops selling Aboriginal art and crafts, South Sea pearls, opals and diamonds. The liveliest part of the waterfront is reachable from the Central Business District via the Esplanade or an elevated walkway.');

-- --------------------------------------------------------

--
-- Table structure for table `flight`
--

CREATE TABLE IF NOT EXISTS `flight` (
  `flight_id` int(11) NOT NULL,
  `departure_date` date NOT NULL,
  `destination_id` int(11) NOT NULL COMMENT 'foreign key referencing DESTINATION table'
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `frequent_flyer`
--

CREATE TABLE IF NOT EXISTS `frequent_flyer` (
  `frequent_flyer_id` int(11) NOT NULL COMMENT 'primary key for frequent flyer table',
  `frequent_flyer_points` int(11) NOT NULL COMMENT 'amount of points for the customer'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='This table contains a list of frequent flyer points for each customer.			 			 		';

-- --------------------------------------------------------

--
-- Table structure for table `seat`
--

CREATE TABLE IF NOT EXISTS `seat` (
  `seat_id` int(11) NOT NULL COMMENT 'primary key for seat table',
  `flight_id` int(11) NOT NULL COMMENT 'foreign key referencing AIRLINE table',
  `class_id` int(11) NOT NULL COMMENT 'foreign key referencing CLASS table',
  `customer_id` int(11) NOT NULL COMMENT 'foreign key referencing CUSTOMER table. Null if seat is free',
  `type` int(11) NOT NULL COMMENT 'Used to represent whether seat is "one way" or "return".'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='Table contatins information about the specific seat on the airline.';

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `credit_card`
--
ALTER TABLE `credit_card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`),
  ADD KEY `state_id` (`state`);

--
-- Indexes for table `destination`
--
ALTER TABLE `destination`
  ADD PRIMARY KEY (`destination_id`);

--
-- Indexes for table `flight`
--
ALTER TABLE `flight`
  ADD PRIMARY KEY (`flight_id`),
  ADD UNIQUE KEY `unique_DepartDest` (`departure_date`,`destination_id`),
  ADD KEY `destination_id` (`destination_id`);

--
-- Indexes for table `frequent_flyer`
--
ALTER TABLE `frequent_flyer`
  ADD PRIMARY KEY (`frequent_flyer_id`);

--
-- Indexes for table `seat`
--
ALTER TABLE `seat`
  ADD PRIMARY KEY (`seat_id`),
  ADD KEY `airline_id` (`flight_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `flight_id` (`flight_id`),
  ADD KEY `flight_id_2` (`flight_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key for the class table',AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `credit_card`
--
ALTER TABLE `credit_card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key of credit card table',AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key of Customer Table',AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `destination`
--
ALTER TABLE `destination`
  MODIFY `destination_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key of destination table',AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `flight`
--
ALTER TABLE `flight`
  MODIFY `flight_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `frequent_flyer`
--
ALTER TABLE `frequent_flyer`
  MODIFY `frequent_flyer_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key for frequent flyer table',AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `seat`
--
ALTER TABLE `seat`
  MODIFY `seat_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key for seat table',AUTO_INCREMENT=31;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `flight`
--
ALTER TABLE `flight`
  ADD CONSTRAINT `destination_fk` FOREIGN KEY (`destination_id`) REFERENCES `destination` (`destination_id`);

--
-- Constraints for table `seat`
--
ALTER TABLE `seat`
  ADD CONSTRAINT `class_fk` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`),
  ADD CONSTRAINT `customer_fk` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`),
  ADD CONSTRAINT `flight_fk` FOREIGN KEY (`flight_id`) REFERENCES `flight` (`flight_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
