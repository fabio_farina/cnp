<?php
    include('includes/db.php');
    $title="Reporting Information - Users";
?>

<html class="whole">
  <?php include('templates/header.php'); ?>
  <body>
    <?php include('templates/navbar.php'); ?>
    <div class="container">
      <div class="content">
        <h2><?php echo $title; ?> </h2>

        <div class="form_large">
          <table>
            <tr>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Frequent Flyer ID</th>
              <th>Frequent Flyer Points</th>
            </tr>
          <?php
            $customers = retrieveCustomerList();


            while($row = mysqli_fetch_assoc($customers)) {
              echo "<tr>";
              echo "  <td>". $row['first_name'] . "</td>";
              echo "  <td>". $row['last_name'] . "</td>";
              echo "  <td>". $row['frequent_flyer_id'] . "</td>";
              echo "  <td>". $row['frequent_flyer_points'] . "</td>";
              echo "</tr>";
            }

          ?>
        </table>
        <input type="button" value="Go Back" onclick="window.location='reports.php'" />
        </div>
      </div>
    </div>
  </body>
</html>

<script>
  $('.navbar:first ul li:eq(2) a').attr("class", "active");
</script>
