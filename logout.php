<?php
  include('includes/db.php');
  $title="Log out";

?>

<html>
    <?php include('templates/header.php'); ?>
<body>
    <?php include('templates/navbar.php'); ?>
    <h2><?php echo $title; ?> </h2>
    <?php
      if(isset($_SESSION["customer_details"])) {
        session_unset();
        session_destroy();
        header('Location: index.php');
      }
    ?>

</body>
</html>
