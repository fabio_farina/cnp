<?php
    include('includes/db.php');
    $title="Customer Information"
?>

<html class="whole">
  <?php include('templates/header.php'); ?>
  <body>
    <?php include('templates/navbar.php'); ?>
    <div class="container">
      <div class="content">
        <h2><?php echo $title; ?> </h2>
        <?php
          if(!isset($_SESSION["customer_details"])) {

            if(isset($_POST["first_name"]) && isset($_POST["last_name"]) && isset($_POST["gender"]) && isset($_POST["dateofbirth"]) && isset($_POST["town"])
                && isset($_POST["state"]) && isset($_POST["postcode"]) && isset($_POST["phone"]) && isset($_POST["email"])) {

                  $first_name = $_POST["first_name"];
                  $last_name = $_POST["last_name"];
                  $gender = $_POST["gender"];
                  $date_of_birth = $_POST["dateofbirth"];
                  $street = $_POST["street"];
                  $town = $_POST["town"];
                  $state = $_POST["state"];
                  $postcode = $_POST["postcode"];
                  $phone = $_POST["phone"];
                  $email = $_POST["email"];

                  $res = createUser($first_name, $last_name, $gender, $date_of_birth, $street, $town, $state, $postcode, $phone, $email);


                  if($res) {
                    $customer = mysqli_fetch_assoc(retrieveCustomer($res["customer_id"]));
                    echo "Registered. Welcome, " . $customer["first_name"];
                    $_SESSION["customer_details"] = $customer;
                  } else {
                    echo "Something went wrong";
                  }

            } else {


        ?>
        <form action="" method="post" class="form_large">
          First Name: <input type="text" name="first_name" id="first_name" /> <br />
          Last Name: <input type="text" name="last_name" id="last_name" /> <br />
          Gender:
          <select name="gender" id="gender">
            <option value="M">Male</option>
            <option value="F">Female</option>
          </select>
          <br />
          Date of Birth: <input type="text" name="dateofbirth" id="datepicker" /> <br />
          Street: <input type="text" name="street" id="street" /><br />
          Town: <input type="text" name="town" id="town" /> <br />
          State: <input type="text" name="state" id="state" /> <br />
          Postcode: <input type="text" name="postcode" id="postcode" /> <br />
          Phone: <input type="text" name="phone" id="phone" /> <br />
          Email: <input type="text" name="email" id="email" /> <br />
          <input type="submit" value="Register" />
        </form>

        <?php
            }
          } else {
            echo "You're already logged in.";
          }

          ?>
        </div>
      </div>

  </body>
</html>
