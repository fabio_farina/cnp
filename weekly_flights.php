<?php
    include('includes/db.php');
    $title="Reporting Information - Weekly Flights"
?>

<html class="whole">
  <?php include('templates/header.php'); ?>
  <body>
    <?php include('templates/navbar.php'); ?>
    <div class="container">
      <div class="content">
        <h2><?php echo $title; ?> </h2>
        <?php
        if(!isset($_GET["startdate"])) {
        ?>
        <form action="" method="get" class="form_large">
          <b>Select Start Date for Week:</b>
          <input type="text" name="startdate" id="datepicker" />
          <input type="Submit" value="Run Report" />
        </form>
        <?php
      } else {
            $date = $_GET["startdate"];
            $res = retrieveWeeklyReport($date);
            $total = 0;
        ?>
        <div class="form_large">
          <h3>Report period:</h3>
          <?php         
            echo $_GET["startdate"];
          ?>
          <table>
            <tr>
              <th>Date</th>
              <th>Amount</th>
            </tr>
          <?php
              while($row = mysqli_fetch_assoc($res)) {
                if($row["cost"] != "") {
                  echo "<tr>";
                  echo "  <td>" . $row["departure"] . "</td>";
                  echo "  <td>$" . number_format($row["cost"],2) . "</td>";
                  echo "</tr>";
                  $total = $total + $row["cost"];
                }
              }
          ?>
          </table>
          <b>Total made for the week:</b> $<?php echo number_format($total,2); ?> <br />
          <?php
            }
          ?>

          <input type="button" value="Go Back" onclick="window.location='reports.php'" />
        </div>
      </div>
    </div>
  </body>
</html>

<script>
  $('.navbar:first ul li:eq(2) a').attr("class", "active");
</script>
