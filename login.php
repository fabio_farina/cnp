<?php
  include('includes/db.php');
  $title="Log in";

?>

<html class="whole">
    <?php include('templates/header.php'); ?>
<body>
    <?php include('templates/navbar.php'); ?>

    <div class="container">
      <div class="content">
        <h2><?php echo $title; ?> </h2>
        <?php
          if($_SESSION["customer_details"] == null) {
            $show_login = true;
            if(isset($_POST["customer_id"])) {
              $customer_id = $_POST["customer_id"];
              $res = mysqli_fetch_assoc(retrieveCustomer($customer_id));

              if($res["customer_id"] == $customer_id) {
                echo "Login successful";
                $show_login = false;
                $_SESSION["customer_details"] = $res;
                header('Location: index.php');
              } else {
                echo "Login failed.";
              }
            }
            if($show_login) {
          ?>
          <form action="login.php" method="post" class="form_large">
            Customer ID: <input type="text" name="customer_id" id="customer_id" />
            <input type="submit" value="Log in">
          </form>
          <?php
            }
          } else {
            echo "You're already logged in, " . $_SESSION["customer_details"]["first_name"];
          }
          ?>
        </div>
    </div>

</body>
</html>
