<?php
    include('includes/db.php');
    $title="Customer Information"
?>

<html class="whole">
  <?php include('templates/header.php'); ?>
  <body>
    <?php include('templates/navbar.php'); ?>

    <div class="container">
      <div class="content">
        <h2><?php echo $title; ?> </h2>
        <?php
          if(isset($_SESSION["customer_details"])) {
            $res = retrieveCustomer($_SESSION["customer_details"]["customer_id"]);
            $details = mysqli_fetch_assoc($res);
        ?>
        <table class="customer_info">
          <tr>
            <td>First Name</td>
            <td><?php echo $details["first_name"]; ?></td>
          </tr>
          <tr>
            <td>Last Name</td>
            <td><?php echo $details["last_name"]; ?></td>
          </tr>
          <tr>
            <td>Gender</td>
            <td><?php echo $details["gender"]; ?></td>
          </tr>
          <tr>
            <td>Date of Birth</td>
            <td><?php echo $details["date_of_birth"]; ?></td>
          </tr>
          <tr>
            <td>Town</td>
            <td><?php echo $details["town"]; ?></td>
          </tr>
          <tr>
            <td>State</td>
            <td><?php echo $details["state"]; ?></td>
          </tr>
          <tr>
            <td>Post Code</td>
            <td><?php echo $details["postcode"]; ?></td>
          </tr>
          <tr>
            <td>Telephone</td>
            <td><?php echo $details["phone"]; ?></td>
          </tr>
          <tr>
            <td>Email Address</td>
            <td><?php echo $details["email"]; ?></td>
          </tr>
          <tr>
            <td>Date Joined</td>
            <td><?php echo $details["join_date"]; ?></td>
          </tr>
          <tr>
            <td>Frequent Flyer Id</td>
            <td><?php echo $details["frequent_flyer_id"]; ?></td>
          </tr>
          <tr>
            <td>Frequent Flyer Points</td>
            <td><?php echo $details["frequent_flyer_points"]; ?></td>
          </tr>
        </table>
        <?php
        } else {
        ?>
        <p>You're currently not logged in.</p>
        <p>Click <a href="login.php">here</a> to log in</p>
        <?php
          }
        ?>
      </div>
    </div>
  </body>
</html>

<script>
  $('.navbar:first ul li:eq(3) a').attr("class", "active");
</script>
