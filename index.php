<?php
  include('includes/db.php');

  $title="Main Page";

?>

<html class="whole">
    <?php include('templates/header.php'); ?>
<body>

    <?php include('templates/navbar.php'); ?>
    <div class="container">
      <div class="content">
        <h2><?php echo $title; ?> </h2>
        <p>
            Canberra National Air a small airline service provider that allows for customers to fly to all major
            cities of Australia. We allow customers to ride both one way or return and currently have two classes
            set up (business and economy). We also have our special "frequent flyer" system where a loyal customer
            is able to pay for a flight entirely using the points they've racked up when flying with us.
        </p>
      </div>
    </div>

</body>

</html>

<script>
  $('.navbar:first ul li:eq(0) a').attr("class", "active");
</script>
